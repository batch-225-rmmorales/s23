// 1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a trainer object using object literals.
// 4. Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)
// - Friends (Object with Array values for properties)
// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// 6. Access the trainer object properties using dot and square bracket notation.
// 7. Invoke/call the trainer talk object method.
// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)
// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// 11. Create a faint method that will print out a message of targetPokemon has fainted.
// 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// 13. Invoke the tackle method of one pokemon object to see if it works as intended.
// 14. Create a git repository named S23.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.

function Trainer (name, age, pokemon, friends) {
    this.name = name;
    this.age = age;
    this.pokemon = pokemon;
    this.friends = friends;
    this.talk = function(){
        console.log('pikachu i choose you')
    }


}

let mors = new Trainer("mors", 37, ["zeus"], []);
console.log(mors);
console.log(typeof mors)

console.log(mors.age);
console.log(mors['pokemon']);

mors.talk();

function Pokemon (name, level, health, attack){
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = attack;
}

let pikachu = new Pokemon('pikachu',1,100,25);
let bulbasaur = new Pokemon('bulbasaur',2,120,35);
let charizard = new Pokemon('charizard',10,350,75);

Pokemon.prototype.faint = function() {
    console.log(this.name + " has fainted");
}

Pokemon.prototype.tackle = function(target) {
    console.log(this.name + " used tackle on " + target.name);
    target.health -= this.attack;
    if (target.health <= 0){
        target.faint();
    }
};
charizard.tackle(pikachu);
charizard.tackle(pikachu);

//ulitin ko ung trainer declaration here after pokemon objects

let vanessa = new Trainer("mors", 37, [pikachu,bulbasaur,charizard], [mors]);